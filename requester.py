# -*- coding: utf-8 -*-

'''
Program to request Sentinel 2 images massively from Copernicus Open Access Hub.

Author: Ausiàs R. T.
'''
# - No res.
#TODO: Demanar imatges amb un fitxer meta4 de sci-hub.
#TODO: Interfície gràfica.
#TODO: Generalització per a qualsevol usuari.

# %% Libraries
# Built-in
import xml.etree.ElementTree as ET
import math as m
import time
from tqdm import tqdm

# Sentinel API
from sentinelsat import SentinelAPI
api = SentinelAPI('user', 'password')

# %% Load and parse data
# Arxiu meta4 de resultats del carret de descàrregues.
with open('products.xml', 'r') as f:
	data = f.read()

root = ET.fromstring(data)

product_names = list()

# Get product names
for child in root:
	product_names.append(child.attrib['name'])

# %% Retrieve availability
n_products = len(product_names)
product_uuid = list()
availability = list()
failed = list()

for i in tqdm(range(n_products)):
# 	print('Image nº:' + str(i))
	try:
		uuid = list(api.query(identifier=product_names[i]).keys())[0]
		product_uuid.append(uuid)
		availability.append(api.is_online(uuid))
	except:
		print('Image nº:' + str(i) + ' failed.')
		failed.append(product_names[i])

# %% Filter and drop online products
offline = [x for x, y in zip(product_uuid, availability) if y == False]

# %% Request offline products 20 by 20 (maximum allowed)
maxp = 19
n_offline = len(offline)
n_packs = m.ceil(n_offline/maxp)
j = 0

# For bigger requests
if n_offline > maxp:
	# Information
	print(str(len(offline)) + " images left.")
	# Split data in a smaller segment
	segment = offline[0:maxp]
	# First segment
	for request in segment:
		print('Request: ' + request)
		# Retry if the server fails
		try:
			api.trigger_offline_retrieval(request)
		except:
			print('Retry: ' + request)
			api.trigger_offline_retrieval(request)
		time.sleep(10)
	
	# Once these have been requested, retrieve the online ones
	print('Waiting...')
	time.sleep(10*60)
	j = maxp + 1
	while j < n_offline -1:
		# Information
		print(str(len(offline)) + " images left.")
		# Check if there is any request online
		for request in segment:
			result = api.is_online(request)
			if result is True:
				print('Image available:' + request)
				# Remove the finished one
				segment.remove(request)
				# Process the next offline product
				print('Request: ' + offline[j])
				# Retry if the server fails
				try:
					api.trigger_offline_retrieval(offline[j])
				except:
					print('Retry: ' + offline[j])
					api.trigger_offline_retrieval(offline[j])
				segment.append(offline[j])
				j = j + 1
		print('Waiting...')
		time.sleep(10*60)
	
	# For smaller requests:
else:
	for request in offline:
		print('Request: ' + request)
		# Retry if the server fails
		try:
			api.trigger_offline_retrieval(request)
		except:
			print('Reintenta: ' + request)
			api.trigger_offline_retrieval(request)
		time.sleep(10)
	
	# Wait
	print('Waiting...')
	time.sleep(10*60)
	
	while len(offline) > 1:
		# Information
		print(str(len(offline)) + " images left.")
		for request in offline:
			result = api.is_online(request)
			if result is True:
				print('Image available:' + request)
				# Remove the finished one
				try:
					segment.remove(request)
				except:
					break
		print('Waiting...')
		time.sleep(10*60)




